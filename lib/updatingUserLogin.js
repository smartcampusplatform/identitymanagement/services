'use strict'

var db = require('../db/dbconfig').dbPromise()

module.exports = function updatingUserLogin(x,y) {
	return new Promise((resolve, reject) => {
		// console.log(x)
		var dataInp = {
			// loginid: x,
			logintoken: x,
			lastlogin: new Date(),
			logininfo: y,
			loginstatus: 0
		}
		db.any('UPDATE loginuser SET login_status = ${loginstatus}, last_login = ${lastlogin} WHERE login_token = ${logintoken} AND login_info = ${logininfo} AND login_status = 1 RETURNING login_userid', dataInp)
			.then(function (data) {
				// console.log(data.length)
				var result
				if(data.length == 1) {
					result = {
						messages: 'user ' + data[0].login_userid + ' logout successfully'
					}
				} else {
					result = {
						messages: 'login first'
					}
				}
				// console.log(result)
				resolve(result)
			})
			.catch(function(err) {
				// console.error('Error in logout user ' + x + ' ', err)
				reject(err)
			})
	})
}
