'use strict'

var db = require('../db/dbconfig').dbPromise()

module.exports = function addingLogin(x,y,z) {
	return new Promise((resolve, reject) => {
		// console.log(x)
		var dataInp = {
			loginakun: x.user_id,
			logintoken: y,
			statuslogin: 1,
			lastlogin: new Date(),
			logininfo: z
		}
		db.none('INSERT INTO loginuser(login_userid, login_token, login_status, last_login, login_info) VALUES (${loginakun}, ${logintoken}, ${statuslogin}, ${lastlogin}, ${logininfo})', dataInp)
			.then(function() {
				var result = {
					message: 'user login successfully',
					data: {
						login_token: y
					}
				}
				// console.log(result)
				resolve(result)
			})
			.catch(function(err) {
				// console.error('Error in adding user to database', err)
				reject(err)
			})
	})
}
