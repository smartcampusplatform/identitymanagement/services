'use strict'

var db = require('../db/dbconfig').dbPromise()

module.exports = function addingUser(x) {
	return new Promise((resolve, reject) => {
		// console.log(x)
		var dataInp = {
			namaakun: x.userid,
			passwordakun: require('crypto').createHash('md5').update(x.userpass).digest('hex'),
			emailakun: x.useremail,
			roleakun: x.userrole,
			statusakun: 1
		}
		db.none('INSERT INTO users(user_id, user_pass, user_email, user_role, user_status) VALUES(${namaakun}, ${passwordakun}, ${emailakun}, ${roleakun}, ${statusakun})', dataInp)
			.then(function() {
				var result = {
					message: 'User registered successfully'
				}
				// console.log(result)
				resolve(result)
			})
			.catch(function(err) {
				// console.error('Error in adding user to database', err)
				reject(err)
			})
	})
}
