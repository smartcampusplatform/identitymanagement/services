'use strict'

var db = require('../db/dbconfig').dbPromise()

module.exports = function loginUser(x,y) {
	return new Promise((resolve, reject) => {
		// console.log(x)
		db.any('SELECT login_userid, login_token, login_status, last_login, login_info FROM loginuser WHERE login_userid = $1 AND login_info = $2 AND login_status = 1', [x,y])
			.then(function (data) {
				// console.log('data: ', data)
				// console.log(data.length)
				var result
				if(data.length > 0) {
					result = {
						messages: 'already login successfully',
						data: {
							login_token: data[0].login_token
						}
					}
				} else {
					result = {
						messages: 'available'
					}
				}
				// console.log(result)
				resolve(result)
			})
			.catch(function(err) {
				// console.error('Error in check login user to database', err)
				reject(err)
			})
	})
}
